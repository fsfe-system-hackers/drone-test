# SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
FROM php:7-apache
ARG DISCOURSE_API_KEY
ARG LDAP_BIND_PW
# Set up PHP files
COPY . /var/www/html
RUN sed -i "s/SECRET_API_KEY/$DISCOURSE_API_KEY/" /var/www/html/config.php
# Set up Apache
COPY 000-default.conf /etc/apache2/sites-enabled/
RUN sed -i "s/LDAP_BIND_PW/$LDAP_BIND_PW/" /etc/apache2/sites-enabled/000-default.conf
RUN a2enmod ldap authnz_ldap
CMD apache2-foreground
